import { Component, OnInit } from '@angular/core';
import { RouteInfo } from '../core/ui';

@Component({
  selector: 'app-modules',
  templateUrl: './modules.component.html',
  styleUrls: ['./modules.component.scss']
})
export class ModulesComponent implements OnInit {
  routes: RouteInfo[] = [
    {
      path: '/dashboard',
      title: 'Dashboard',
      type: 'sub',
      icontype: 'dashboard',
      collapse: 'dashboard',
      children: [
        { path: 'main', title: 'Principal', ab: 'PR' },
        { path: 'special', title: 'Especial', ab: 'ES' }
      ]
    },
    {
      path: '/consulta',
      title: 'Consulta',
      type: 'link',
      icontype: 'search',
      collapse: 'consulta'
    },
    {
      path: '/testes-integracao',
      title: 'Testes de Integração',
      type: 'link',
      icontype: 'search',
      collapse: 'testIntegracao'
    }
  ];

  constructor() {}

  ngOnInit() {}
}
