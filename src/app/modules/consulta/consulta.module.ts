import { ConsultaComponent } from './consulta.component';
import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';

const declarations = [ConsultaComponent];

@NgModule({
  declarations: [...declarations],
  imports: [CommonModule],
  exports: [...declarations]
})
export class ConsultaModule {}
