import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { TestesIntegracaoComponent } from './testes-integracao.component';

describe('TestesIntegracaoComponent', () => {
  let component: TestesIntegracaoComponent;
  let fixture: ComponentFixture<TestesIntegracaoComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ TestesIntegracaoComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(TestesIntegracaoComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
