import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';

import { TestesIntegracaoComponent } from './testes-integracao.component';

const declarations = [TestesIntegracaoComponent];

@NgModule({
  declarations: [...declarations],
  imports: [CommonModule],
  exports: [...declarations]
})
export class TestesIntegracaoModule {}
