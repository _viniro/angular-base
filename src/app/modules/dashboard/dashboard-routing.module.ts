import { MainDashboardComponent } from './main-dashboard/main-dashboard.component';
import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { DashboardComponent } from './dashboard.component';
import { SpecialDashboardComponent } from './special-dashboard/special-dashboard.component';

const routes: Routes = [
  {
    path: '',
    component: DashboardComponent,
    children: [
      {
        path: 'main',
        component: MainDashboardComponent
      },
      {
        path: 'special',
        component: SpecialDashboardComponent
      },
      {
        path: '',
        redirectTo: 'main'
      }
    ]
  }
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule]
})
export class DashboardRoutingModule {}
