import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';

import { DashboardRoutingModule } from './dashboard-routing.module';
import { DashboardComponent } from './dashboard.component';
import { MainDashboardComponent } from './main-dashboard/main-dashboard.component';
import { SpecialDashboardComponent } from './special-dashboard/special-dashboard.component';
import { UiModule } from 'src/app/core/ui/ui.module';

const declarations = [DashboardComponent];

@NgModule({
  declarations: [
    ...declarations,
    MainDashboardComponent,
    SpecialDashboardComponent
  ],
  imports: [CommonModule, DashboardRoutingModule, UiModule]
})
export class DashboardModule {}
