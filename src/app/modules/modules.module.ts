import { TestesIntegracaoModule } from './testes-integracao/testes-integracao.module';
import { ModulesRoutingModule } from './modules-routing.module';
import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { ModulesComponent } from './modules.component';
import { UiModule } from '../core/ui/ui.module';
import { ConsultaModule } from './consulta/consulta.module';

@NgModule({
  declarations: [ModulesComponent],
  imports: [
    CommonModule,
    ModulesRoutingModule,
    UiModule,
    ConsultaModule,
    TestesIntegracaoModule
  ]
})
export class ModulesModule {}
