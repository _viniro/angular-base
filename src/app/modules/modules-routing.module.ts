import { ModulesComponent } from './modules.component';
import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { ConsultaComponent } from './consulta/consulta.component';
import { TestesIntegracaoComponent } from './testes-integracao/testes-integracao.component';

const routes: Routes = [
  {
    path: '',
    component: ModulesComponent,
    children: [
      {
        path: 'dashboard',
        loadChildren: './dashboard/dashboard.module#DashboardModule'
      },
      {
        path: 'consulta',
        component: ConsultaComponent
      },
      {
        path: 'testes-integracao',
        component: TestesIntegracaoComponent
      },
      {
        path: '',
        redirectTo: '/dashboard/main'
      }
    ]
  }
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule]
})
export class ModulesRoutingModule {}
