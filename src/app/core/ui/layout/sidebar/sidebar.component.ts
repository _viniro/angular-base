import { Component, OnInit, Input, Output, EventEmitter } from '@angular/core';
import { RouteInfo } from '../model';
import PerfectScrollbar from 'perfect-scrollbar';
import * as $ from 'jquery';

@Component({
  selector: 'app-sidebar',
  templateUrl: './sidebar.component.html',
  styleUrls: ['./sidebar.component.scss']
})
export class SidebarComponent implements OnInit {
  @Output() logOff: EventEmitter<any> = new EventEmitter();
  @Input() routes: RouteInfo[];
  @Input() title: string;

  constructor() {}

  public menuItems: any[];

  isMobileMenu() {
    if ($(window).width() > 991) {
      return false;
    }
    return true;
  }

  logOffClicked(): void {
    this.logOff.emit();
  }

  ngOnInit() {
    this.menuItems = this.routes.filter((menuItem) => menuItem);
  }
  updatePS(): void {
    if (window.matchMedia(`(min-width: 960px)`).matches && !this.isMac()) {
      const elemSidebar: HTMLElement = document.querySelector(
        '.sidebar .sidebar-wrapper'
      );
      const ps = new PerfectScrollbar(elemSidebar, {
        wheelSpeed: 2,
        suppressScrollX: true
      });
    }
  }
  isMac(): boolean {
    let bool = false;
    if (
      navigator.platform.toUpperCase().indexOf('MAC') >= 0 ||
      navigator.platform.toUpperCase().indexOf('IPAD') >= 0
    ) {
      bool = true;
    }
    return bool;
  }
}
