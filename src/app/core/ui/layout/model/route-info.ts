import { RouteChildrenInfo } from './route-children-info';
export interface RouteInfo {
  path: string;
  title: string;
  type: string;
  icontype: string;
  collapse?: string;
  children?: RouteChildrenInfo[];
}
