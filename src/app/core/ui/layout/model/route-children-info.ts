export interface RouteChildrenInfo {
  path: string;
  title: string;
  ab: string;
  type?: string;
}
